# Efficiency in data processing. data.table basics

We will briefly go through the db-benchmark [^footnote] report to see
performance-wise state of data processing tools for operations such as
aggregation and join.
Then we will discuss basic concepts of data.table. How we can use
extended version of data.frame `[` operator to achieve flexibility of
expressing data processing operations in comprehensive and concise
way.

[^footnote]: https://h2oai.github.io/db-benchmark
