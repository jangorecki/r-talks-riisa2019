---
title: "Efficiency in data processing"
author: Jan Gorecki
date: December 26, 2019
output:
  xaringan::moon_reader:
    seal: false
    css: [default, fonts.css]
    self_contained: true
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
---

```{r render, eval=FALSE, include=FALSE}
#Rscript -e 'rmarkdown::render("index.Rmd")' && xdg-open index.html
```

class: center, middle, page-font-26

![data.table](https://avatars2.githubusercontent.com/u/7824179?s=200&v=4)

# Efficiency in data processing

[_R@IISA 2019_](https://r-iisa2019.rbind.io), 26th December, 2019, Mumbai, India

Jan Gorecki

```{r init, include=FALSE}
writeLines(c(
  "body { font-family: serif; }",
  "h1, h2 { font-family: serif; font-weight: normal; }",
  "h3, h4 { font-family: serif; }",
  ".remark-code, .remark-inline-code { font-family: monospace; }",
  ".remark-slide-number { display: none; }",
  ".page-font-18 { font-size: 18px; !important; }",
  ".page-font-22 { font-size: 22px; !important; padding: 1em 4em 1em 4em; }",
  ".page-font-24 { font-size: 24px; !important; padding: 1em 4em 1em 4em; }",
  ".page-font-26 { font-size: 26px; !important; padding: 1em 4em 1em 4em; }",
  ".page-font-28 { font-size: 28px; !important; padding: 1em 4em 1em 4em; }",
  ".page-font-36 { font-size: 36px; !important; padding: 1em 4em 1em 4em; }"
), "fonts.css")
library(data.table)
```

???

0m

---

# Is R still competitive for data processing tasks?

???

+1m = 1m

- competitive in terms of features (IO, formats, data cleaning)
- competitive in ease to learn
- competitive in expressiveness (vs SQL)
- competitive in time and memory resources: ?

--

```{r timeline, echo=FALSE, fig.width=12, fig.height=6.75, fig.align='center', eval=FALSE, include=TRUE}
# we use static image, not from chunk, so pdf export catch it also
fread("tech,name,date,r
python,python,1990,0
R,R,1993,1
julia,julia,2012,0
spark,spark,2014,0
ClickHouse,ClickHouse,2016,0
cuDF,CUDA GPU DataFrames,2018,0"
)[, date:=as.Date(paste0(date,"-01-01"))
][, invisible({
  ylim = c(0,1)
  plot(0, 0, type="n", frame.plot=FALSE, axes=FALSE, xlab="", ylab="", xlim=range(date), ylim=ylim)
  par(new=TRUE)
  plot(date, rep(c(0.25,0.5), .N/2), type="h", frame.plot=FALSE,  axes=FALSE, xlab="", ylab="", col="grey", xlim=range(date), ylim=ylim, main="technologies", cex.main=2)
  text(date, rep(c(0.25,0.5), .N/2), tech, cex=2.25, srt=45, pos=3, xpd=TRUE, col=ifelse(r==1, "darkblue", "black"), font=ifelse(r==1, 2, 1))
  axis(1, at=date, labels=FALSE, pos=0)
  text(x=date, y=-0.125, labels=format(date, "%Y"), srt=45, pos=3, xpd=TRUE, cex=1.75)
  legend("top", legend=name, xpd=TRUE, bty="n", cex=1.75, text.col=ifelse(r==1, "darkblue", "black"), text.font=ifelse(r==1, 2, 1))
})] -> nul
```
.center[![tech](https://user-images.githubusercontent.com/3627377/71087087-582d7400-21c1-11ea-98c9-71333530a628.png)]

---

# Is R still competitive for data processing tasks?

```{r timeline2, echo=FALSE, fig.width=12, fig.height=6.75, fig.align='center', eval=FALSE, include=TRUE}
# we use static image, not from chunk, so pdf export catch it also
fread("tech,date,r
data.table,2006,1
pandas,2008,0
julia DF,2013,0
dplyr,2014,1
spark,2014,0
dask,2015,0
ClickHouse,2016,0
(py)datatable,2018,0
cuDF,2018,0"
)[, date:=as.Date(paste0(date,"-01-01"))
][, invisible({
  ylim = c(0,1)
  plot(0, 0, type="n", frame.plot=FALSE, axes=FALSE, xlab="", ylab="", xlim=range(date), ylim=ylim)
  par(new=TRUE)
  plot(date, rep(c(0.25,0.5), ceiling(.N/2))[seq_len(.N)], type="h", frame.plot=FALSE,  axes=FALSE, xlab="", ylab="", col="grey", xlim=range(date), ylim=ylim, main="solutions", cex.main=2)
  text(date, rep(c(0.25,0.5), ceiling(.N/2))[seq_len(.N)], tech, cex=2.25, srt=45, pos=3, xpd=TRUE, col=ifelse(r==1, "darkblue", "black"), font=ifelse(r==1, 2, 1))
  axis(1, at=date, labels=FALSE, pos=0)
  text(x=date, y=-0.125, labels=format(date, "%Y"), srt=45, pos=3, xpd=TRUE, cex=1.75)
})] -> nul
```
.center[![sol](https://user-images.githubusercontent.com/3627377/71087197-9b87e280-21c1-11ea-95f0-b5b4d17bbbd6.png)]

???

+0.5m = 1.5m

- in the mean time many other tools like map reduce, not really efficient but more focused on scaling up data sizes in distributed environment

---

class: page-font-36

# Database-like ops benchmark

- benchmark runs routinely, upgrades software, re-run benchmarking script
- fully reproducible, open source
- focused on one-machine environment
- continuously developed; new tasks, data sizes, solutions are being added.

.center[[h2oai.github.io/db-benchmark](https://h2oai.github.io/db-benchmark)]

???

+1.5m = 3m

- single machine because they are getting bigger and bigger over time
- dask, spark, clickhouse can run on distributed environments

---

class: page-font-26

## groupby

### questions

.pull-left[
#### basic questions

- sum
- mean
- sum and mean
- 4 of 5 grouping by single column
- 1 of 5 grouping by two columns

Originally in [2014 grouping benchmark](https://github.com/Rdatatable/data.table/wiki/Benchmarks-%3A-Grouping)
]
.pull-right[
#### new advanced questions

- median, sd
- range v1-v2: `max(v1)-min(v2)`
- top 2 rows: `order(.); head(.,2)`
- regression: `cor(v1, v2)^2`
- count
- grouping by 6 columns
]

???

+2m = 5m

- due to popular demand db-benchmark has been created

---

class: page-font-28

## groupby

### data

```
      id1    id2          id3   id4   id5   id6    v1    v2      v3
   <fctr> <fctr>       <fctr> <int> <int> <int> <int> <int>   <num>
1:  id046  id007 id0000043878    51    10 59276     1     1 96.8126
2:  id041  id026 id0000068300    12    58 78315     4     1 83.5654
```

.pull-left[
#### size

```
1e7 rows:   0.5 GB
1e8 rows:   5   GB
1e9 rows:  50   GB
```
]
.pull-right[
#### cardinality

- balanced
- unbalanced
- heavily unbalanced
]

???

+2m = 7m

- 120GB memory machine used now
- should be enough but it is not always enough
- dask on-disk parquet

---

class: page-font-24

## groupby

#### solution version

- automatically to recent devel

  data.table, (py)datatable

- automatically to recent stable

  pandas, dplyr, dask, spark, julia DataFrames

- manually to recent stable

  CUDA GPU DataFrames, ClickHouse

#### solution syntax

Syntax of each solution is included on the benchmark plot, just next to its timing bar.

???

+2m = 9m

- python - not as easy to upgrade to devel versions as in R
- dplyr - stable because devel is not stable enough
- cudf - upgrade as re-install from scratch
- clickhouse - upgrade with apt-get

---

class: page-font-22

## groupby

### timings
.pull-left[
#### run 1st, 2nd

Each query is run twice and both timings are presented.
]
.pull-right[
#### timing bar cut off

Timing bar of individual run is cut off if it is too long. Using max _spark_'s timing +20% as a threshold.
]
#### script timeout

Each solution benchmark script is terminated if it takes too long. Where _too long_ is defined as:

- 1 hour for 0.5 GB data
- 2 hours for 5 GB data
- 3 hours for 50 GB data

for _groupby_ benchmark. _join_ benchmark timeouts are double those for _groupby_.

???

+1.5m = 10.5m

- discuss slide 1.5m

+3.5m = 14m

- preview benchplot 3.5m
- mention that solutions that failed are moved to the bottom
- mention values are still reported where timing bar is cut off

---

class: page-font-22

## join

### questions

#### basic questions

- join on _integer_ or _factor_
- _inner_ and _outer_ join
- RHS join data of size _small_, _medium_, and _big_

#### advanced questions

Join on mutliple columns and other less trivial join cases to be added.

### solutions

Same as for _groupby_ benchmark, except for ClickHouse yet.

???

+1m = 15m

- RHS sizes are labels that define size relation to LHS size

---

class: page-font-18

## join

### data

```
     id1   id2     id3    id4    id5       id6       v1
   <int> <int>   <int> <fctr> <fctr>    <fctr>    <num>
1:     8  2149 7609766    id8 id2149 id7609766 89.03174
2:     4  4831 9001786    id4 id4831 id9001786 83.71212
```

#### size
.pull-left[LHS
```
1e7 rows:   0.5 GB
1e8 rows:   5   GB
1e9 rows:  50   GB
```
]
.pull-right[RHS
```
small:   LHS/1e6
medium:  LHS/1e3
big:     LHS
```
]

#### cardinality

- id1, id4 - low
- id2, id5 - medium
- id3, id6 - high

???

+2m = 17m

- cardinality can be observed in head of data
- RHS small for LHS 1e7 is just 10 rows table
- RHS big for LHS 1e7 is 1e7 rows table
- RHS small for LHS 1e9 is 1000 rows table
- RHS big for LHS 1e9 is 1e9 rows table
- RHS big is a _big-to-big_ join
- around 90% of rows ar matching in each case
- 1e9 is an out of memory on 128GB machine, unless solution can compute on-disk

---

class: page-font-28

# benchmark conclusion

- time is not the most important factor but just one of many
- most important are correctness and capability to finish the task
- there are many other factors, some of them not easy to measure or present, or even not possible to measure because they are subjective
  - memory usage
  - lines of code
  - code readability
  - API stability
  - timings stability
  - maintenance effort
  - dependencies
  - license
  - ...

???

+2m = 19m

- time is easy to measure
- benchmark were revealing bugs in different solutions
- memory efficiency often translates to capability to compute the task
- lines of code - if you have to read 10 lines rather then 1 then risk of missing something increases
- timings stability - for same data
- timings stability - for different cardinality data
- more dependencies increase the risk of failure

---

# data.table basics

#### extends `[` data.frame method

```r
DF[i, j]
DT[i, j, by, ...]
```

#### in SQL

```r
FROM [WHERE, SELECT, GROUP BY]
DT   [i,     j,      by]
```

#### example

```r
library(data.table)
DF <- iris
DT <- as.data.table(iris)
```

???

+1m = 19m

- before we proceed to example and would like to brielfy explain...

---

class: page-font-22

## what is so special about data.table?

???

+3m = 22m

- `order` - later incoporated to base R
- memory
  - grouping allocates memory for biggest group and reuse it
  - join algorithm uses _sorting_ instead of _hashing_
  - join and grouping at once: `by=.EACHI`
  - advoid in-memory copies `set*`, `setkey`, `setDT`, `setcolorder`, `:=`
- chaining ~ piping, from base R `[` design

--

- syntax
  
  - concise and consistent
  - fast to read and fast to type
  - corresponding to SQL queries
  ```
  FROM[where|orderby, select, groupby]
  ```

--

- faster speed
  
  - focus on implementation using efficient algorithms, some later incorporated into base R itself
  - using indexes, keys (clustered index)
  - using fewer in-memory copies also saves time
  
--

- less memory usage - not only related to _by reference_ operations but in general!
  
  - memory efficient algorithms
  - join and grouping at once do not materialize intermediate join results
  - _by reference_ operations avoid unnecessary in-memory copies

---

## data.table syntax

### subset

#### rows

```r
DF[DF$Petal.Width > 2.1,]
subset(DF, Petal.Width > 2.1)

DT[Petal.Width > 2.1]
```

#### columns

```r
DF[, c("Petal.Width", "Petal.Length", "Species")]

DT[, .(Petal.Width, Petal.Length, Species)]
DT[, c("Petal.Width", "Petal.Length", "Species")]
```

???

+2m = 24m

- less repetitions
- less error prone
- no trailing comma
- less quoting
- support `select` interface inside `[`
- prints head and tail

---

## data.table syntax

### mean on columns

```r
data.frame(
  Petal.Width = mean(DF$Petal.Width),
  Petal.Length = mean(DF$Petal.Length)
)
with(
  DF,
  data.frame(Petal.Width = mean(Petal.Width), Petal.Length = mean(Petal.Length))
)
as.data.frame(lapply(
  DF[, c("Petal.Width", "Petal.Length")],
  mean
))

DT[, .(Petal.Width = mean(Petal.Width), Petal.Length = mean(Petal.Length))]
DT[, lapply(.SD, mean), .SDcols = c("Petal.Width", "Petal.Length")]
```

???

- first example we are ignoring data.frame class features at all by using vectors
- `with` API in column selection
- `.SDcols` Subset Data column selection

+2m = 26m

---

## data.table syntax

### mean by group

```r
tmp1 <- split(DF, DF$Species)
tmp2a <- lapply(tmp1, function(df) data.frame(
  mean(df$Petal.Width),
  mean(df$Petal.Length)
))
do.call("rbind", tmp2a)
tmp2b <- lapply(tmp1, function(df) as.data.frame(lapply(
  df[, c("Petal.Width", "Petal.Length")],
  mean
)))
do.call("rbind", tmp2b)

DT[, .(mean(Petal.Width), mean(Petal.Length)), Species]
DT[, lapply(.SD, mean), by = Species,
   .SDcols = c("Petal.Width", "Petal.Length")]
```

???

+2m = 28m

- `lapply(tmp, ...)` can use `with` way
- or to use pass columns as vectors we get nested `lapply`
- in DT we just add `by` argument

---

## data.table syntax

### subset, mean and sum by group

```r
subDF <- DF[DF$Sepal.Width > 3.0 & DF$Sepal.Length > 4.0,]
tmp1 <- split(subDF, subDF$Species)
tmp2b <- lapply(tmp1, function(df) as.data.frame(c(
  lapply(df[, c("Petal.Width", "Petal.Length")], mean),
  lapply(df[, c("Petal.Width", "Petal.Length")], sum)
)))
do.call("rbind", tmp2b)

DT[Sepal.Width > 3.0 & Sepal.Length > 4.0,
   c(lapply(.SD, mean), lapply(.SD, sum)),
   by = Species,
   .SDcols = c("Petal.Width", "Petal.Length")]
```

???

+2m = 30m

- tmp1b will be easy, just type more
- easy to use `lapply(.SD)` to apply same function to multiple columns
- `select` in `i`
- `with` in `j`

---

## data.table syntax

### join

```r
SDF <- data.frame(
  Species = c("setosa","versicolor","virginica"),
  ID = c(101L, 102L, 103L)
)
SDT <- as.data.table(SDF)
```

#### outer join

```r
merge(DF, SDF, by = "Species", all.y = TRUE)

DT[SDT, on = "Species"]
```

#### inner join

```r
merge(DF, SDF, by = "Species")

DT[SDT, on = "Species", nomatch = NULL]
```

???

+3m = 33m

- `nomatch` arg control behaviour of inner/outer
- we can still provide second `j` expression to evaluate during the join

---

## data.table syntax

### R's `[` chaining

```r
letters[2:6][1:4][2:3]   ## letters[3:4]
```

#### same R's `[` chaining utilized in data.table

```
FROM[sub-query][outer-query][...][most-outer-query]
```

```r
DT[Sepal.Width > 3.0 & Sepal.Length > 4.0,
   .(mean_pet_len = mean(Petal.Length)),
   Species
   ][mean_pet_len > 3.0
     ]
```

???

+1m = 34m

- R's chaining is like pipes but not able to refer to self
- in DF we have to create tmp and then subset it because there is no way to reference self
- because of local scope in `[` data.table we can use piping without extra implementing such a feature, it comes for free

---

class: page-font-28

# thanks to H2O.ai

H2O.ai is funding a lot of data.table development. We are very thankful for this contribution to R ecosystem.

## what is H2O.ai?

H2O.ai is best known for its open source machine learning library H2O.  
H2O is parallelized, distributed, supports various ML algorithms, automatic ML, and produces high accuracy models.  
It is written in java but has interfaces in multiple languages, [including R](https://cloud.r-project.org/package=h2o).

???

+1m = 35m

- Driverless AI commercial product

---

class: center, middle, page-font-28

# thank you, questions?

[r-datatable.com](http://r-datatable.com)<br/>
<br/>
[h2o.ai](https://www.h2o.ai)<br/>
<br/>
[datatable.h2o.ai](http://datatable.h2o.ai)<br/>
<br/>
<br/><br/><br/><br/><br/><br/>
`j.gorecki _in_ wit.edu.pl`<br/>
<br/>
[github.com/jangorecki](https://github.com/jangorecki) | [gitlab.com/jangorecki](https://gitlab.com/jangorecki)

???

+0m = 35m

